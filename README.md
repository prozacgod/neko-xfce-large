Just a docker image based on the Neko webtop.

This is using m1k1o's (https://github.com/m1k1o/neko) base image 

This unashamedly copies the Dockerfile from the existing xfce4 desktop and adds
    * xfce4-goodies
    * pavucontrol (allows the pulseaudio volume control widget to work on the desktop audio)
    * inkscape & gimp (collaborative art project anyone?? :P) 
    * firefox-esr (just so the desktop has a browser)

My thought here is that including a good smattering of software will keep the desire down to install or update things in this system.  So you would come back to here or rebuild it once in a while to get updates.  In that case you may want to mount your home folder in the host somewhere to preserve settings.

Pull/Feature requests for good tools accepted, I would like to keep the image in the 600mb-800mb range.
