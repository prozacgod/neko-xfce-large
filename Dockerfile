ARG BASE_IMAGE=m1k1o/neko:base
FROM $BASE_IMAGE

#
# install xfce
RUN set -eux; apt-get update; \
    apt-get install -y --no-install-recommends sudo \
      # desktop environment
      xfce4 xfce4-terminal xfce4-goodies \
      # desktop utilities
      pavucontrol \
      # art programs
      inkscape gimp \
      # browser
      firefox-esr;

# 
# add user to sudoers
RUN  usermod -aG sudo neko; \
    echo "neko:neko" | chpasswd; \
    echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers;

#
# copy configuation files
COPY supervisord.conf /etc/neko/supervisord/xfce.conf